module gitlab.com/gitlab-org/incubation-engineering/apm/datadog-sandbox

go 1.16

require (
	github.com/ClickHouse/clickhouse-go v1.4.5
	github.com/jmoiron/sqlx v1.3.4
)
