package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"time"

	_ "github.com/ClickHouse/clickhouse-go"
	"github.com/jmoiron/sqlx"
)

var (
	listenFlag    = flag.String("listen", ":5678", "address and port to listen")
	clickHouseDSN = flag.String("clickHouseDSN", "", "ClickHouse DNS connection string")
)

type SeriesModel struct {
	Timestamp time.Time
	Host      string
	Metric    string
	Value     float64
	Tags      []string
}

type SeriesBody struct {
	Series []SeriesItem
}

type SeriesItem struct {
	Host     string
	Interval int64
	Metric   string
	Points   [][2]float64
	Tags     []string
	Type     string
}

func main() {
	flag.Parse()

	if *clickHouseDSN == "" {
		log.Fatal("Please specify clickHouseDNS argument")
	}

	db, err := sqlx.Open("clickhouse", *clickHouseDSN)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	if err := db.Ping(); err != nil {
		log.Fatal(err)
	}

	if err := initDB(db); err != nil {
		log.Fatal(err)
	}

	http.HandleFunc("/api/v1/series", seriesStore(db))
	log.Fatal(http.ListenAndServe(*listenFlag, nil))
}

func initDB(db *sqlx.DB) error {
	if _, err := db.Exec(`
		CREATE DATABASE IF NOT EXISTS metrics
	`); err != nil {
		return err
	}

	_, err := db.Exec(`
		CREATE TABLE IF NOT EXISTS metrics.series
		(
			timestamp DateTime('UTC'),
			host String,
			metric String,
			value Float64,
			tags Array(String)
		) ENGINE = MergeTree()
		ORDER BY timestamp
		PRIMARY KEY tuple()
	`)
	return err
}

func seriesStore(db *sqlx.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var series SeriesBody
		if err := json.NewDecoder(r.Body).Decode(&series); err != nil {
			httpError(w, "decoding JSON: %v", err)
			return
		}
		models := []SeriesModel{}
		for _, s := range series.Series {
			for _, p := range s.Points {
				models = append(models, SeriesModel{
					Timestamp: time.Unix(int64(p[0]), 0),
					Host:      s.Host,
					Metric:    s.Metric,
					Value:     p[1],
					Tags:      s.Tags,
				})
			}
		}

		if len(models) > 0 {
			tx, err := db.Beginx()
			if err != nil {
				httpError(w, "being tx: %v", err)
				return
			}
			stmt, err := tx.PrepareNamed(`INSERT INTO metrics.series (timestamp, host, metric, value, tags)
				VALUES (:timestamp, :host, :metric, :value, :tags)`)
			if err != nil {
				httpError(w, "bulk insert series: %v", err)
				return
			}
			defer stmt.Close()

			for _, m := range models {
				if _, err := stmt.Exec(m); err != nil {
					_ = tx.Rollback()
					httpError(w, "tx exec: %v", err)
					return
				}
			}
			if err := tx.Commit(); err != nil {
				httpError(w, "tx commit: %v", err)
				return
			}
		}

		fmt.Fprintln(w, `{"status":"ok"}`)
	}
}

func httpError(w http.ResponseWriter, msg string, err error) {
	log.Printf(msg, err)
	http.Error(w, err.Error(), http.StatusBadRequest)
}
