# DataDog Testing Sandbox

A docker-compose environment to allow testing of the DataDog agent.

The sandbox prevents external networking, so we can isolate requests and audit them. 
We also use a CoreDNS service to audit outgoing DNS requests that aren't going to our services.

Full (mostly) DataDog agent config can be explored here - https://github.com/DataDog/datadog-agent/blob/main/pkg/config/config_template.yaml

Uses [mock-server](https://www.mock-server.com/) to capture HTTP traffic so we can easily investigate the traffic from the agent.

## Usage

Use `asdf install` in the root directory to install supporting tools.

[mkcert](https://github.com/FiloSottile/mkcert) is used to set up local trusted certificates.
Run `make certs` once to generate certificates for the services.

`docker-compose up -d` to start the agent and mock services.

See `./docker-compose.yml` for more details of the setup.

View the main `mock-server` web UI at `http://localhost:1080/mockserver/dashboard`.

View the CoreDNS logs with `docker-compose logs dns-proxy`.

## Setup

The DataDog agent uses multiple endpoints for various features, specifically the main site, process data, logs, traces and appsec. By default each feature goes to a subdomain of `datadoghq.com`. These are overridden with environment variables, `DD_LOGS_CONFIG_LOGS_DD_URL` etc.

For this reason we've got multiple `mock-server` containers, one for each `*_DD_URL`. That way we can investigate those logs separately.