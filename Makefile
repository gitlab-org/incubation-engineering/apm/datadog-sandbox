.PHONY: certs
certs:
	-mkdir secrets
	mkcert -key-file secrets/key.pem -cert-file secrets/cert.pem localhost $(shell docker-compose ps --services)
	chmod 644 secrets/key.pem
	cp $(shell mkcert -CAROOT)/rootCA.pem secrets/root.pem

.PHONY: compose-up
compose-up:
	docker-compose up --build -d

.PHONY: compose-recreate
compose-recreate:
	docker-compose up -d --force-recreate

.PHONY: compose-down
compose-down:
	docker-compose down